import logging
from typing import List

import pymongo
from bson.objectid import ObjectId

logger = logging.getLogger(__name__)


class MongoLsv:
    def __init__(self, host: str = "0.0.0.0", port: int = 27017) -> pymongo.MongoClient:
        try:
            self.client = pymongo.MongoClient(f"mongodb://{host}:{port}/")
        except Exception as e:
            logger.error(e)

    def list_dbs(self) -> list:
        return self.client.list_database_names()

    def list_collections(self, db_name: str) -> list:
        return self.client[db_name].list_collection_names()

    def get_records_from_collection(
        self,
        db_name: str,
        collection: str,
        limit_value: int = 10,
        query_search: dict = {},
    ) -> List[dict]:
        return list(
            self.client[db_name][collection].find(query_search).limit(limit_value)
        )

    def create_new_record_in_collection(
        self, db_name: str, collection: str, record: dict
    ) -> dict:
        record = self.client[db_name][collection].insert_one(record)
        return {"transaction_code": "OK", f"{collection}": record.inserted_id}

    def update_record_in_collection(
        self, db_name: str, collection: str, record_query: dict, record_new_value: dict
    ) -> dict:
        set_value = {"$set": record_new_value}
        self.client[db_name][collection].update_one(record_query, set_value)
        return {"transaction_code": "OK"}

    def delete_record_in_collection(
        self, db_name: str, collection: str, record_id: str
    ) -> dict:
        self.client[db_name][collection].delete_one({"_id": ObjectId(record_id)})
        return {"transaction_code": "OK"}
