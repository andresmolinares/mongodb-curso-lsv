from dataclasses import asdict, dataclass
from datetime import datetime

@dataclass
class VegetablesTypes:
    uuid: str
    name: str
    
    def to_dict(self) -> dict:
        return asdict(self)
    
@dataclass
class Vegetables:
    uuid: str
    name: str
    type_vegetable: VegetablesTypes
    description: str
    color: str
    
    def to_dict(self) -> dict:
        return asdict(self)
    