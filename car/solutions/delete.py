from bson.objectid import ObjectId  # noqa
import sys
sys.path.append("..")
from databases.dbclient import MongoLsv

mongo_lsv = MongoLsv()
db_name = "andresmolinares"

print(mongo_lsv.delete_record_in_collection(
    db_name=db_name,
    collection="vegetables",
    record_id="62afcf55df5792627bef5f74"
))
print('*** Verdura eliminada ***')
print(
    mongo_lsv.get_records_from_collection(
        db_name=db_name, collection="vegetables"
    )
)
print("***********************************")

print(mongo_lsv.delete_record_in_collection(
    db_name=db_name,
    collection="vegetables_types",
    record_id="62afa808e37eb038b88f1776"
))

print('*** Tipo de verdura eliminada ***')
print(
    mongo_lsv.get_records_from_collection(
        db_name=db_name, collection="vegetables_types"
    )
)
print("***********************************")