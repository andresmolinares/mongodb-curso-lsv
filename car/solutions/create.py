from bson.objectid import ObjectId  # noqa
import sys
sys.path.append("..")
from databases.dbclient import MongoLsv

mongo_lsv = MongoLsv()
db_name = "andresmolinares"

#Create Vegetable types

print(mongo_lsv.create_new_record_in_collection(
    db_name=db_name,
    collection="vegetables_types",
    record={"name": "Hortalizas"}
))

print(mongo_lsv.create_new_record_in_collection(
    db_name=db_name,
    collection="vegetables_types",
    record={"name": "Verduras de raiz"}
))

print(mongo_lsv.create_new_record_in_collection(
    db_name=db_name,
    collection="vegetables_types",
    record={"name": "Verduras de bulbo"}
))

print(mongo_lsv.create_new_record_in_collection(
    db_name=db_name,
    collection="vegetables_types",
    record={"name": "Hortalizas C"}
))

print('*** Tipos de verduras creados ***')
print(
    mongo_lsv.get_records_from_collection(
        db_name=db_name, collection="vegetables_types"
    )
)
print("***********************************")
# Create Vegetables
print(mongo_lsv.create_new_record_in_collection(
    db_name=db_name,
    collection="vegetables",
    record={"name": "Tomate", "type_vegetable": [ObjectId('62afa808e37eb038b88f1773')],
            "description":"Ingrediente vegetal usado en ensaladas.", "color":"Rojo"}
))
print(mongo_lsv.create_new_record_in_collection(
    db_name=db_name,
    collection="vegetables",
    record={"name": "Lechuga", "type_vegetable": [ObjectId('62afa808e37eb038b88f1773')],
            "description": "Planta herbacea cultivada en invernaderos.", "color":"Verde"} 
))

print(mongo_lsv.create_new_record_in_collection(
    db_name=db_name,
    collection="vegetables",
    record={"name": "Cebolla", "type_vegetable": [ObjectId('62afa808e37eb038b88f1775')],
            "description":"Planta herbacea bienal.", "color":"Blanco"}
))
print(mongo_lsv.create_new_record_in_collection(
    db_name=db_name,
    collection="vegetables",
    record={"name": "Ajo", "type_vegetable": [ObjectId('62afa808e37eb038b88f1775')],
            "description":"Verdura muy usdada en la cocina. Cultivado en amplias zonas del mundo.", "color":"Blanco"}
))

print(mongo_lsv.create_new_record_in_collection(
    db_name=db_name,
    collection="vegetables",
    record={"name": "Zanahoria", "type_vegetable": [ObjectId('62afa808e37eb038b88f1774')],
            "description":"Considerada la verdura mas importante y de mayor consumo.", "color":"Naranja"}
))

print(mongo_lsv.create_new_record_in_collection(
    db_name=db_name,
    collection="vegetables",
    record={"name": "Apio2", "type_vegetable": [ObjectId('62afa808e37eb038b88f1774')],
            "description":"Especie de verdura perteneciente a la familia de las apiáceas.", "color":"Verde"}
))

print(mongo_lsv.create_new_record_in_collection(
    db_name=db_name,
    collection="vegetables",
    record={"name": "Brocoli", "type_vegetable": [ObjectId('62afa808e37eb038b88f1776')],
            "description":"Es una planta de la familia de las brasicáceas", "color":"Verde"}
))

print('*** Verduras creadas ***')
print(
    mongo_lsv.get_records_from_collection(
        db_name=db_name, collection="vegetables"
    )
)
print("***********************************")