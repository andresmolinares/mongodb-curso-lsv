from bson.objectid import ObjectId  # noqa
import sys
sys.path.append("..")
from databases.dbclient import MongoLsv

mongo_lsv = MongoLsv()
db_name = "andresmolinares"

print(mongo_lsv.update_record_in_collection(
    db_name=db_name,
    collection="vegetables_types",
    record_query={'_id': ObjectId('62afa808e37eb038b88f1776')},
    record_new_value={'name': 'Hortalizas Cruciferas'}
))

print('*** Tipo de verdura editada ***')
print(
    mongo_lsv.get_records_from_collection(
        db_name=db_name, collection="vegetables_types"
    )
)
print("***********************************")

print(mongo_lsv.update_record_in_collection(
    db_name=db_name,
    collection="vegetables",
    record_query={'_id': ObjectId('62afcf55df5792627bef5f73')},
    record_new_value={'name': 'Apio'}
))

print('*** Verdura editada ***')
print(
    mongo_lsv.get_records_from_collection(
        db_name=db_name, collection="vegetables"
    )
)
print("***********************************")
